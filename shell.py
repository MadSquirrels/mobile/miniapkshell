import frida, sys, argparse

jscode = """
rpc.exports =
{
    command(cmd)
    {
      Java.perform(function()
        {
          var Runtime = Java.use('java.lang.Runtime');
          var String = Java.use('java.lang.String');
          var InputStreamReader = Java.use('java.io.InputStreamReader');
          var BufferedReader = Java.use('java.io.BufferedReader');

          function read(response, func)
          {
            var b = BufferedReader.$new(response);
            while(true)
            {
              var line = b.readLine();
              if (line == null)
                break;
              func(line);
            }
          }

          var p = Runtime.getRuntime().exec(cmd);
          var response = InputStreamReader.$new(p.getInputStream());
          read(response, console.log);
          var response = InputStreamReader.$new(p.getErrorStream());
          read(response, console.error);
          return;
        });
    }
};
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description='Open a shell in application Context')
    parser.add_argument('--application', '-a' , help='application name',
            default='Gadget')
    parser.add_argument('--device', '-d' , help='device name')

    args = parser.parse_args()
    application = args.application

    if not args.device:
        process = frida.get_usb_device().attach(application)
    else:
        for d in frida.get_device_manager().enumerate_devices():
            if d.id == args.device:
                process = d.attach(application)
    if not "process" in locals():
        print(f"device {args.device} not found")
        sys.exit(1)

    script = process.create_script(jscode)
    script.load()
    while True:
        try:
            cmd = input('>')
            script.exports.command(cmd)
        except EOFError:
            print("bye")
            sys.exit(0)
