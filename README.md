# README

## GENERAL INFO

Project: miniAPKshell  
Contributors: MadSquirrels  
License: GNU General Public License v3.0  
Version: v1.0  
Date: 02-37-21  


## GOAL

The goal of this script is to have a mini shell on the context of the
application targeted. It is useful when you didn't have a root access on the
smartphone or simulate the real access of the application.

## REQUIREMENT

You nee to install:
 - pip3 install -r requirements.txt

You will need to inject a frida-gadget on the apk targeted with
https://gitlab.com/MadSquirrels/mobile/apkpatcher or install a frida-server on
the android phone.

## Example

```
python3.7 shell.py -d emulator-5554
>ls
ls: .: Permission denied
>ls /system/
app
bin
build.prop
compatibility_matrix.xml
etc
fake-libs
fonts
framework
lib
lost+found
manifest.xml
media
priv-app
tts
usr
vendor
>bye
```

## Donation

If you want to support me <3  

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=FPG4QZWGUTPXW&currency_code=EUR)
